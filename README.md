# Get Started

# Pre-req

1. Node.js > 10
2. Npm > 6

# Get Running

```
$ npm install
$ npm build
$ npm start
```

# Enviroment variables

## All this are for the FE app, get from FB AccountKit

```
CHILAKIL_ACCOUNT_KIT_ID
CHILAKIL_ACCOUNT_KIT_SECRET
CHILAKIL_ACCOUNT_KIT_LOGIN_URL
CHILAKIL_ACCOUNT_KIT_REDIRECT_URL
CHILAKIL_ACCOUNT_KIT_LOGOUT_URL
CHILAKIL_ACCOUNT_KIT_API_VERSION
```

## This are for the API Project

API Key is a 32 char encryiption key that is shared with the API project.

```
CHILAKIL_API_HOST
CHILAKIL_API_KEY
CHILAKIL_API_VERSION
```

## This are for the API Project

Cookie key is also an encryption key, this has no lenght requirement but make it not 123

```
CHILAKIL_COOKIE_KEY
CHILAKIL_GRAPHQL_URL
```
