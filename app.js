const csrf = require('csurf');
const morgan = require('morgan');
const helmet = require('helmet');
const express = require('express');
const bodyParser = require('body-parser');
const favicon = require('express-favicon');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');

// ################################################################
// EXPRESS APP CONFIG
// ################################################################
const app = express();

app.use(helmet());
app.use(bodyParser.json());
app.use(cookieParser('tlespolcuatlo'));
app.use(favicon(`${__dirname}/static/favicon.png`));
app.use(morgan('tiny'));

// ################################################################
// SESSION CONFIG
// ################################################################
// 30 days, 24 hours, 60 min, 60 sec, 1000 milisec
const TTL = 30 * 24 * 60 * 60 * 1000;
const secret = process.env.CHILAKIL_COOKIE_KEY;

app.use(csrf({ cookie: true }));
app.use(
  cookieSession({
    secret,
    name: 'chtks',
    maxAge: TTL,
  }),
);

module.exports = app;
