import { getSessionVars } from './utils';

export default defaults => ({
  session: getSessionVars(defaults.session),
});
