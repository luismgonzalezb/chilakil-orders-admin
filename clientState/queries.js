import gql from 'graphql-tag';

// SERVER
export const ALL_MENU_ITEMS = gql`
  query allMenuItems($skip: Int!, $limit: Int!) {
    allMenuItems(skip: $skip, limit: $limit) {
      menuItemId
      name
    }
  }
`;

export const UPDATE_USER = gql`
  mutation updateUser($name: String, $lastname: String, $email: String) {
    updateUser(name: $name, lastname: $lastname, email: $email) {
      name
      lastname
      email
    }
  }
`;

export const GET_RESTAURANTS = gql`
  query restaurants {
    restaurants {
      restaurantId
      name
      description
    }
  }
`;

export const ADD_RESTAURANT = gql`
  mutation addRestaurant($name: String, $description: String) {
    addRestaurant(name: $name, description: $description) {
      name
      description
    }
  }
`;

// CLIENT
export const LOGIN = gql`
  mutation login($session: session) {
    login(session: $session) @client
  }
`;

export const IS_LOGGED_IN = gql`
  query {
    session @client {
      isLoggedIn
    }
  }
`;

export const SESSION = gql`
  query {
    session @client {
      isLoggedIn
      user {
        name
        lastname
        email
        role {
          roleId
          role
        }
      }
    }
  }
`;
