export default {
  Mutation: {
    login: (_, { session }, { cache }) => {
      cache.writeData({ data: { session } });
      return null;
    },
  },
};
