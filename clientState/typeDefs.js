// Define your types here.
const typeDefs = `
  type role {
    roleId: ID!
    role: String!
    active: Boolean!
  }
  type user {
    name: String
    lastname: String
    email: String
    role: role
  }
  type session {
    isLoggedIn: Boolean!
    user: user
  }
  type Mutation {
    login(session: session!)
    logout()
  }
`;

export default typeDefs;
