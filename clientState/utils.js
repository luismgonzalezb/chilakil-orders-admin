const getRole = session =>
  session.user
    ? {
        __typename: 'role',
        ...session.user.role,
      }
    : {
        __typename: 'role',
        role: '',
      };

const getUser = session =>
  session && {
    __typename: 'user',
    ...session.user,
    role: getRole(session),
  };

export const getSessionVars = session => ({
  __typename: 'session',
  ...session,
  user: getUser(session),
});

export default getSessionVars;
