import React from 'react';
import { object } from 'prop-types';
import UserMenu from './UserMenu';

const HeaderMenu = ({ session }) => (
  <nav className="navbar">{session.isLoggedIn && <UserMenu />}</nav>
);

HeaderMenu.propTypes = {
  session: object.isRequired,
};

export default HeaderMenu;
