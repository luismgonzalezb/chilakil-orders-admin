import React from 'react';
import Link from 'next/link';

const UserMenu = () => (
  <div>
    <span>User</span>
    <ul>
      <li>
        <Link href="/profile">
          <a href="/profile" data-testid="profile-link">
            Perfil
          </a>
        </Link>
      </li>
      <li>
        <a href="/logout" data-testid="logout-link">
          Salir
        </a>
      </li>
    </ul>
  </div>
);

export default UserMenu;
