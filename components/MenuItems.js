import React from 'react';
import { Query } from 'react-apollo';
import { ALL_MENU_ITEMS } from '../clientState/queries';

const MenuItems = () => (
  <Query query={ALL_MENU_ITEMS} variables={{ skip: 0, limit: 10 }}>
    {({ loading, error, data }) => {
      if (loading) return 'Loading...';
      if (error) return `Error! ${error.message}`;
      return data.allMenuItems.map(item => (
        <div key={item.menuItemId}>{item.name}</div>
      ));
    }}
  </Query>
);

export default MenuItems;
