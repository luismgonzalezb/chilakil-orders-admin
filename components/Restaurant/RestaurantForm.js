import React from 'react';
import { object, func } from 'prop-types';
import { Form, Text } from 'informed';

const RestaurantForm = ({ restaurant, onSubmit, onCancel }) => (
  <Form onSubmit={values => onSubmit(values)}>
    <div>
      <label htmlFor="name">Name:</label>
      <Text field="name" id="name" initialValue={restaurant.name} />
      <label htmlFor="description">Description:</label>
      <Text
        field="description"
        id="description"
        initialValue={restaurant.description}
      />
    </div>
    <button type="submit">Save</button>
    <button type="button" onClick={onCancel}>
      Cancel
    </button>
  </Form>
);

RestaurantForm.propTypes = {
  restaurant: object,
  onSubmit: func.isRequired,
  onCancel: func.isRequired,
};

RestaurantForm.defaultProps = {
  restaurant: {},
};

export default RestaurantForm;
