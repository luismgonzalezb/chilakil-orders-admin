import React from 'react';
import { array } from 'prop-types';

const RestaurantListDisplay = ({ restaurants }) => (
  <ul>
    {restaurants.map(restaurant => (
      <li key={restaurant.restaurantId}>{restaurant.name}</li>
    ))}
  </ul>
);

RestaurantListDisplay.propTypes = {
  restaurants: array.isRequired,
};

export default RestaurantListDisplay;
