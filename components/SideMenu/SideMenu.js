import React from 'react';
import { object } from 'prop-types';
import { withRouter } from 'next/router';
import cx from 'classnames';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import leftMenu from '../../constants/leftMenu';

const SideMenu = ({ router }) => (
  <nav className="side-menu open clear">
    <ul>
      {leftMenu.map(item => (
        <li key={item.path}>
          <Link href={item.path}>
            <a
              className={cx('side-link', {
                active: item.path === router.route,
              })}
              href={item.path}
            >
              <FontAwesomeIcon icon={['fal', item.icon]} />
              <span>{item.label}</span>
            </a>
          </Link>
        </li>
      ))}
    </ul>
  </nav>
);

SideMenu.propTypes = {
  router: object.isRequired,
};

export default withRouter(SideMenu);
