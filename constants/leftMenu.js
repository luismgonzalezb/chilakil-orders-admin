const leftMenu = [
  { path: '/', label: 'Inicio', icon: 'tachometer' },
  { path: '/restaurants', label: 'Restaurants', icon: 'tachometer' },
  { path: '/menus', label: 'Menus', icon: 'tachometer' },
  { path: '/about', label: 'Acerca De', icon: 'tachometer' },
];

export default leftMenu;
