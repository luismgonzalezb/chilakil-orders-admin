import React from 'react';
import { Query } from 'react-apollo';

import HeaderMenu from '../components/Header/HeaderMenu';
import { IS_LOGGED_IN } from '../clientState/queries';

const Header = () => (
  <Query query={IS_LOGGED_IN}>
    {({ data, loading }) => !loading && <HeaderMenu session={data.session} />}
  </Query>
);

export default Header;
