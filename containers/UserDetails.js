import React from 'react';
import { func } from 'prop-types';
import { Query, compose, graphql } from 'react-apollo';
import { Form, Text } from 'informed';

import { getSessionVars } from '../clientState/utils';
import { SESSION, UPDATE_USER, LOGIN } from '../clientState/queries';

const UserDetails = ({ updateUser, login }) => {
  const onSubmit = (values, session) => {
    updateUser({ variables: { ...values } });
    login({
      variables: {
        session: getSessionVars({
          ...session,
          user: { ...session.user, ...values },
        }),
      },
    });
  };

  return (
    <Query query={SESSION}>
      {({ loading, data: { session } }) =>
        !loading && (
          <Form onSubmit={values => onSubmit(values, session)}>
            <div>
              <label htmlFor="name">
                First name:
                <Text field="name" id="name" initialValue={session.user.name} />
              </label>
              <label htmlFor="lastname">
                Last name:
                <Text
                  id="lastname"
                  field="lastname"
                  initialValue={session.user.lastname}
                />
              </label>
              <label htmlFor="email">
                Email:
                <Text
                  id="email"
                  field="email"
                  type="email"
                  initialValue={session.user.email}
                />
              </label>
            </div>
            <button type="submit">Update</button>
          </Form>
        )}
    </Query>
  );
};

UserDetails.propTypes = {
  updateUser: func.isRequired,
  login: func.isRequired,
};

export default compose(
  graphql(UPDATE_USER, { name: 'updateUser' }),
  graphql(LOGIN, { name: 'login' }),
)(UserDetails);
