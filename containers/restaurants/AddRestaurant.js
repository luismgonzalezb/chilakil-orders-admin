import React, { Fragment } from 'react';
import { func } from 'prop-types';
import { compose, graphql } from 'react-apollo';
import { ADD_RESTAURANT } from '../../clientState/queries';
import RestaurantForm from '../../components/Restaurant/RestaurantForm';

const AddRestaurant = ({ onSuccess, onCancel, addRestaurant }) => {
  const onSubmit = async values => {
    const restult = await addRestaurant({
      variables: values,
    });
    if (!restult.errors) onSuccess();
  };

  return (
    <>
      <RestaurantForm
        onSubmit={values => onSubmit(values, addRestaurant)}
        onCancel={onCancel}
      />
    </>
  );
};

AddRestaurant.propTypes = {
  onCancel: func.isRequired,
  onSuccess: func.isRequired,
  addRestaurant: func.isRequired,
};

export default compose(graphql(ADD_RESTAURANT, { name: 'addRestaurant' }))(
  AddRestaurant,
);
