import React from 'react';
import { Query } from 'react-apollo';

import RestaurantListDisplay from '../../components/Restaurant/RestaurantListDisplay';
import { GET_RESTAURANTS } from '../../clientState/queries';

const RestaurantList = () => (
  <Query query={GET_RESTAURANTS}>
    {({ data, loading }) =>
      !loading && <RestaurantListDisplay restaurants={data.restaurants} />}
  </Query>
);

export default RestaurantList;
