import React from 'react';
import { oneOfType, arrayOf, node } from 'prop-types';
import Header from '../containers/Header';
import SideMenu from '../components/SideMenu';

import './Main.scss';

const Main = ({ children }) => [
  <SideMenu key="side-menu" />,
  <main key="side-content" className="side-content">
    <Header />
    <div className="main-content">{children}</div>
  </main>,
];

Main.propTypes = {
  children: oneOfType([arrayOf(node), node]).isRequired,
};

export default Main;
