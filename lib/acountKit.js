/* global AccountKit */
import { loadScript } from './browser-utils';

// login callback
const loginCallback = (response, resolve, reject) => {
  if (response.status === 'PARTIALLY_AUTHENTICATED') {
    const { code, state } = response;
    const URL = process.env.CHILAKIL_ACCOUNT_KIT_LOGIN_URL;
    // Send code to server to exchange for access token
    fetch(URL, {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'csrf-token': state,
      },
      body: JSON.stringify({ code }),
    })
      .then(resp => {
        if (resp.status === 200 && resp.statusText === 'OK') {
          return resp.json();
        }
        throw resp;
      })
      .then(session => resolve({ user: session.user, isLoggedIn: true }))
      .catch(() => reject('SERVER ERROR'));
  } else {
    reject(response.status);
  }
};

export const initAccountKit = () => {
  const token = window.localStorage.getItem('token');
  const state = encodeURIComponent(token);
  AccountKit.init({
    state,
    debug: true,
    fbAppEventsEnabled: true,
    appId: process.env.CHILAKIL_ACCOUNT_KIT_ID,
    version: process.env.CHILAKIL_ACCOUNT_KIT_API_VERSION,
    redirect: process.env.CHILAKIL_ACCOUNT_KIT_REDIRECT_URL,
  });
};

export const emailLogin = (callback, emailAddress) =>
  window.AccountKit.login('EMAIL', { emailAddress }, callback);

export const smsLogin = (callback, phoneNumber, countryCode = '+1') =>
  window.AccountKit.login('PHONE', { countryCode, phoneNumber }, callback);

export const loginWithFB = type =>
  new Promise((resolve, reject) => {
    const cb = response => loginCallback(response, resolve, reject);
    return type === 'PHONE' ? smsLogin(cb, '') : emailLogin(cb, '');
  });

const initFB = () => {
  window.AccountKit_OnInteractive = initAccountKit;
};

export const addAccountKit = () =>
  loadScript('https://sdk.accountkit.com/en_US/sdk.js', initFB);
