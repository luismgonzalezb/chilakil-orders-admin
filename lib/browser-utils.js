import Router from 'next/router';

export const redirect = ({ res }, target) =>
  res ? res.redirect(target) : Router.replace(target);

export const isAuth = ctx =>
  !process.browser
    ? ctx.req.isAuthenticated()
    : window.localStorage.getItem('isLoggedIn') === 'true';

export const getCsrfToken = ctx =>
  !process.browser ? ctx.req.csrfToken() : window.localStorage.getItem('token');

export const getUser = ctx =>
  !process.browser
    ? ctx.req.user
    : JSON.parse(window.localStorage.getItem('user'));

export const setLocalStorage = ({ session: { isLoggedIn, user, token } }) => {
  window.localStorage.setItem('isLoggedIn', isLoggedIn);
  window.localStorage.setItem('user', JSON.stringify(user));
  if (token) {
    window.localStorage.setItem('token', token);
  }
};

export const loadScript = (src, onload, async = true, defer = true) => {
  const tag = document.createElement('script');
  tag.async = async;
  tag.defer = defer;
  tag.src = src;
  tag.onload = onload;
  document.body.appendChild(tag);
};
