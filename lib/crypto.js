const crypto = require('crypto');

const ENCRYPTION_KEY = process.env.CHILAKIL_API_KEY; // Must be 256 bytes (32 characters)
const IV_LENGTH = 16; // For AES, this is always 16

const encrypt = text => {
  const iv = crypto.randomBytes(IV_LENGTH);
  const key = Buffer.from(ENCRYPTION_KEY);
  const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return `${iv.toString('base64')}:${encrypted.toString('base64')}`;
};

const decrypt = text => {
  const textParts = text.split(':');
  const iv = Buffer.from(textParts.shift(), 'base64');
  const encryptedText = Buffer.from(textParts.join(':'), 'base64');
  const key = Buffer.from(ENCRYPTION_KEY);
  const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
};

module.exports = { decrypt, encrypt };
