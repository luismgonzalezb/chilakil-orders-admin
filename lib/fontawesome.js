import { library } from '@fortawesome/fontawesome-svg-core';
import { faStroopwafel, faTachometer } from '@fortawesome/pro-light-svg-icons';

export default () => library.add(faStroopwafel, faTachometer);
