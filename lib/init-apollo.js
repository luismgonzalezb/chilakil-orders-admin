import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { ApolloLink } from 'apollo-link';
import { withClientState } from 'apollo-link-state';

import { defaults, resolvers, typeDefs } from '../clientState';

let apolloClient = null;

function create(initialServerState, initialClientState, token) {
  const cache = new InMemoryCache().restore(initialServerState || {});
  const stateLink = withClientState({
    cache,
    defaults: defaults(initialClientState),
    resolvers,
    typeDefs,
  });
  const httpLink = new HttpLink({
    uri: process.env.CHILAKIL_GRAPHQL_URL, // Server URL (must be absolute)
    credentials: 'same-origin', // Additional fetch() options like `credentials` or `headers`
    headers: { 'csrf-token': token },
  });
  return new ApolloClient({
    link: ApolloLink.from([stateLink, httpLink]),
    ssrMode: !process.browser,
    connectToDevTools: process.browser,
    cache,
  });
}

export default function initApollo(
  initialServerState,
  initialClientState,
  token,
) {
  if (!process.browser) {
    return create(initialServerState, initialClientState, token);
  }
  if (!apolloClient) {
    apolloClient = create(initialServerState, initialClientState, token);
  }

  return apolloClient;
}
