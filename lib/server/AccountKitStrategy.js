const PassportStrategy = require('passport-strategy');
const fetch = require('isomorphic-fetch');

/**
 * `AccountKitStrategy` constructor.
 */
class AccountKitStrategy extends PassportStrategy {
  constructor(_options, _verify) {
    const options = _options || {};
    const apiVersion = options.apiVersion || 'v1.1';

    super(options);

    if (typeof _verify !== 'function')
      throw new TypeError('You should provide verify function');
    if (!options.appId) throw new TypeError('You should provide app id');
    if (!options.appSecret)
      throw new TypeError('You should provide app secret');

    this.name = 'accountkit';
    this.verify = _verify;
    this.apiVersion = apiVersion;
    this.accessTokenField = options.accessTokenField || 'code';
    this.meEndpointBaseUrl = `https://graph.accountkit.com/${apiVersion}/me`;
    this.tokenExchangeBaseUrl = `https://graph.accountkit.com/${apiVersion}/access_token`;
    this.appId = options.appId;
    this.appSecret = options.appSecret;
  }

  static lookup(req, field) {
    return (
      (req.body && req.body[field]) ||
      (req.query && req.query[field]) ||
      (req.headers && (req.headers[field] || req.headers[field.toLowerCase()]))
    );
  }

  authenticate(req) {
    const accessToken = AccountKitStrategy.lookup(req, this.accessTokenField);

    if (!accessToken)
      return this.fail({
        message: `You should provide ${this.accessTokenField}`,
      });

    return this.loadUserProfile(accessToken, (error, profile) => {
      if (error) return this.error(error);

      const verified = (user, msg, _error) => {
        if (_error) return this.error(_error);
        if (!user) return this.fail(msg);
        return this.success(user, msg);
      };

      return this.verify(profile, verified, req);
    });
  }

  loadUserProfile(accessToken, done) {
    const AccessToken = ['AA', this.appId, this.appSecret].join('|');
    const params = {
      grant_type: 'authorization_code',
      code: accessToken,
      access_token: AccessToken,
    };
    const querystring = Object.keys(params).reduce(
      (agg, key) => agg.concat(`${key}=${encodeURIComponent(params[key])}&`),
      '',
    );

    // exchange tokens
    const tokenExchangeUrl = `${this.tokenExchangeBaseUrl}?${querystring}`;

    fetch(tokenExchangeUrl, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(resp => resp.json())
      .then(tokenData => {
        // get account details at /me endpoint
        const meEndpointUrl = `${this.meEndpointBaseUrl}?access_token=${tokenData.access_token}`;
        return fetch(meEndpointUrl, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        });
      })
      .then(resp => resp.json())
      .then(meData => {
        done(null, meData);
      })
      .catch(err => done(err));
  }
}

module.exports = AccountKitStrategy;
