const fetch = require('isomorphic-fetch');
const { encrypt } = require('../crypto');
const JwtToken = require('./JwtToken');

const API_KEY = process.env.CHILAKIL_API_KEY;

class ChilakilApiClient {
  constructor() {
    this.host = process.env.CHILAKIL_API_HOST;
    this.version = process.env.CHILAKIL_API_VERSION;
    this.baseUrl = `${this.host}/${this.version}`;
  }

  static getHeaders(token) {
    return {
      'Content-Type': 'application/json',
      Authorization: token ? `Bearer ${token}` : null,
    };
  }

  static makeApiCall({ url, method, body, token }) {
    return fetch(url, {
      method,
      headers: ChilakilApiClient.getHeaders(token),
      body,
    });
  }

  // USER ENDPOINTS
  async createOrGetUserByAccountkitId(accountKitId, phone) {
    const response = await ChilakilApiClient.makeApiCall({
      method: 'POST',
      url: `${this.baseUrl}/admin/users/fb`,
      body: JSON.stringify({
        phone,
        accountKitId,
      }),
    });
    if (response.ok) return response.json();
    throw new Error(response.status);
  }

  async getUser(token) {
    const response = await ChilakilApiClient.makeApiCall({
      token,
      method: 'GET',
      url: `${this.baseUrl}/admin/users`,
    });
    if (response.ok) return response.json();
    throw new Error(response.status);
  }

  async updateUser(token, values) {
    const response = await ChilakilApiClient.makeApiCall({
      token,
      method: 'PUT',
      url: `${this.baseUrl}/admin/users`,
      body: JSON.stringify(values),
    });
    if (response.ok) return response.json();
    throw new Error(response.status);
  }

  async getSessionJwtToken(user, req) {
    const jwttoken = await this.getToken(user, req);
    req.session.jwttoken = jwttoken;
    return jwttoken.token;
  }

  async getToken(user, req) {
    const now = new Date();
    const jwttoken = req.session && req.session.jwttoken;
    if (jwttoken && new Date(jwttoken.ttl) > now) {
      return jwttoken;
    }
    const userKey = encrypt(
      `${user.userId}:${user.accountKitId}:${user.phone}`,
    );
    const response = await ChilakilApiClient.makeApiCall({
      method: 'POST',
      url: `${this.baseUrl}/auth`,
      body: JSON.stringify({
        userKey,
        apiKey: API_KEY,
      }),
    });
    if (response.ok) {
      const token = await response.json();
      return JwtToken(token);
    }
    throw new Error(response.status);
  }

  // RESTAURANT ENDPOINTS
  async getRestaurants(token) {
    const response = await ChilakilApiClient.makeApiCall({
      token,
      method: 'GET',
      url: `${this.baseUrl}/admin/restaurants`,
    });
    if (response.ok) return response.json();
    throw new Error(response.status);
  }

  async addRestaurant(token, values) {
    const response = await ChilakilApiClient.makeApiCall({
      token,
      method: 'POST',
      body: JSON.stringify(values),
      url: `${this.baseUrl}/admin/restaurants`,
    });
    if (response.ok) return response.json();
    throw new Error(response.status);
  }

  // MENU EMDPOINTS
  async getMenuItems() {
    const response = await ChilakilApiClient.makeApiCall({
      method: 'GET',
      url: `${this.baseUrl}/menuitems`,
    });
    if (response.ok) return response.json();
    throw new Error(response.status);
  }
}

function getChilakilApiClient() {
  if (!this.client) {
    this.client = new ChilakilApiClient();
  }
  return this.client;
}

module.exports = {
  ChilakilApiClient,
  getChilakilApiClient,
};
