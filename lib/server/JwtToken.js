const TTL = 30 * 60 * 1000;

const addTTL = ttl => {
  const now = new Date();
  return new Date(now.getTime() + ttl);
};

const JwtToken = ({ token }, ttl = TTL) => ({
  token,
  ttl: addTTL(ttl),
});

module.exports = JwtToken;
