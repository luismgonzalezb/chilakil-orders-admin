module.exports = {
  Query: {
    allMenuItems: (_source, { skip, limit }, { dataSources }) =>
      dataSources.chilakilApi.getMenuItems(skip, limit),
    menuItem: (_source, { id }, { dataSources }) =>
      dataSources.chilakilApi.getMenuItems(id),
    restaurants: (_source, values, { dataSources, token }) =>
      dataSources.chilakilApi.getRestaurants(token),
  },
  Mutation: {
    updateUser: (_source, { name, lastname, email }, { dataSources, token }) =>
      dataSources.chilakilApi.updateUser(token, { name, lastname, email }),
    addRestaurant: (_source, { name, description }, { dataSources, token }) =>
      dataSources.chilakilApi.addRestaurant(token, { name, description }),
  },
};
