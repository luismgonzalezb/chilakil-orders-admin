const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type restaurant {
    restaurantId: ID!
    name: String!
    description: String
  }
  type role {
    roleId: ID!
    role: String!
    active: Boolean!
  }
  type user {
    name: String
    lastname: String
    email: String
    phone: String!
    role: role
  }
  type ingredient {
    ingredientId: ID!
    name: String!
    alergenic: Boolean!
  }
  type menuItem {
    menuItemId: ID!
    name: String!
    description: String!
    price: Float
    ingredients: [ingredient]
  }
  type Query {
    allMenuItems(skip: Int!, limit: Int!): [menuItem]
    menuItem(id: ID!): menuItem
    restaurants: [restaurant]
  }
  type Mutation {
    updateUser(name: String, lastname: String, email: String): user
    addRestaurant(name: String, description: String): restaurant
  }
`;

module.exports = typeDefs;
