const passport = require('passport');
const { getChilakilApiClient } = require('./ChilakilApiClient');

const chilakilClient = getChilakilApiClient();

// ################################################################
// CONFIGURE PASSPORT AUTH
// ################################################################
const AccountKitStrategy = require('./AccountKitStrategy');

passport.use(
  new AccountKitStrategy(
    {
      appId: process.env.CHILAKIL_ACCOUNT_KIT_ID,
      appSecret: process.env.CHILAKIL_ACCOUNT_KIT_SECRET,
      apiVersion: process.env.CHILAKIL_ACCOUNT_KIT_API_VERSION,
    },
    async (user, verify) => {
      try {
        const nuser = await chilakilClient.createOrGetUserByAccountkitId(
          user.id,
          user.phone.number,
        );
        verify(nuser);
      } catch (err) {
        verify(null, 'Error On Client', err);
        throw err;
      }
    },
  ),
);
passport.serializeUser(({ userId, accountKitId, phone }, done) =>
  done(null, { userId, accountKitId, phone }),
);
passport.deserializeUser(async (req, _user, done) => {
  try {
    const token = await chilakilClient.getSessionJwtToken(_user, req);
    const user = await chilakilClient.getUser(token);
    done(null, user);
  } catch (err) {
    done(err, null);
    throw err;
  }
});

module.exports = passport;
