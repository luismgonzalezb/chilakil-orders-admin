/* eslint react/jsx-props-no-spreading: 0 */
import React from 'react';
import Head from 'next/head';
import { object } from 'prop-types';
import { getDataFromTree } from 'react-apollo';
import {
  isAuth,
  getUser,
  redirect,
  getCsrfToken,
  setLocalStorage,
} from './browser-utils';

import initApollo from './init-apollo';

export default App =>
  class Apollo extends React.Component {
    static displayName = 'withApollo(App)';

    static propTypes = {
      session: object,
      apolloState: object,
      apolloClient: object,
    };

    static defaultProps = {
      session: null,
      apolloState: {},
      apolloClient: null,
    };

    constructor(props) {
      super(props);
      const { session } = this.props;
      this.apolloClient =
        props.apolloClient ||
        initApollo(props.apolloState.data, { session }, props.session.token);
    }

    componentDidMount() {
      setLocalStorage(this.props);
    }

    static async getInitialProps(appCtx) {
      const { Component, router, ctx } = appCtx;
      let appProps = {};
      const apolloState = {};
      if (App.getInitialProps) {
        appProps = await App.getInitialProps(appCtx);
      }
      const user = getUser(ctx);
      const token = getCsrfToken(ctx);
      const isLoggedIn = isAuth(ctx);
      const session = {
        isLoggedIn,
        token,
        user,
      };
      appProps = {
        ...appProps,
        session,
        pageProps: {
          ...appProps.pageProps,
          session,
        },
      };
      if (Component.secure && !isLoggedIn) {
        redirect(ctx, '/login');
      } else if (router.route === '/login' && isLoggedIn) {
        redirect(ctx, '/');
      } else {
        const apolloClient = initApollo(null, { session }, token);
        try {
          await getDataFromTree(
            <App
              {...appProps}
              router={router}
              Component={Component}
              apolloClient={apolloClient}
              apolloState={apolloState}
            />,
          );
        } catch (error) {
          console.error('Error while running `getDataFromTree`', error);
        }
        if (!process.browser) {
          Head.rewind();
        }
        apolloState.data = apolloClient.cache.extract();
      }
      return { ...appProps, apolloState };
    }

    render() {
      const { props } = this;
      return <App {...props} apolloClient={this.apolloClient} />;
    }
  };
