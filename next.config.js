/* eslint import/no-extraneous-dependencies: 0 */
const webpack = require('webpack');
const withSass = require('@zeit/next-sass');

module.exports = withSass({
  webpack(config) {
    config.plugins.push(
      new webpack.EnvironmentPlugin([
        'CHILAKIL_ACCOUNT_KIT_LOGIN_URL',
        'CHILAKIL_ACCOUNT_KIT_ID',
        'CHILAKIL_ACCOUNT_KIT_API_VERSION',
        'CHILAKIL_ACCOUNT_KIT_REDIRECT_URL',
        'CHILAKIL_GRAPHQL_URL',
      ]),
    );
    return config;
  },
  sassLoaderOptions: {
    includePaths: ['node_modules'],
  },
});
