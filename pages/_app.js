import React from 'react';
import App, { Container } from 'next/app';
import { ApolloProvider } from 'react-apollo';

import { addAccountKit } from '../lib/acountKit';
import withServer from '../lib/withServer';
import fontawesome from '../lib/fontawesome';

class MyApp extends App {
  constructor(props) {
    super(props);
    fontawesome();
  }

  componentDidMount() {
    const {
      session: { isLoggedIn },
    } = this.props;
    if (!isLoggedIn) {
      addAccountKit();
    }
  }

  render() {
    const { Component, pageProps, apolloClient } = this.props;
    return (
      <Container>
        <ApolloProvider client={apolloClient}>
          <Component {...pageProps} />
        </ApolloProvider>
      </Container>
    );
  }
}

export default withServer(MyApp);
