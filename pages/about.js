import React from 'react';
import Main from '../layouts/Main';

const About = () => (
  <Main>
    <p>About Page</p>
  </Main>
);

About.secure = true;

export default About;
