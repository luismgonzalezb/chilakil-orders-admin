import React from 'react';
import Main from '../layouts/Main';

const Index = () => (
  <Main>
    <h1>Chilakil en Linea</h1>
  </Main>
);

Index.secure = true;

export default Index;
