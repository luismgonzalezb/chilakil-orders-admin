import React from 'react';
import { func } from 'prop-types';
import { compose, graphql } from 'react-apollo';
import { loginWithFB } from '../lib/acountKit';
import { getSessionVars } from '../clientState/utils';
import { LOGIN } from '../clientState/queries';

const Login = ({ login }) => {
  const popLogin = () =>
    loginWithFB('PHONE').then(async session => {
      const result = await login({
        variables: { session: getSessionVars(session) },
      });
      if (!result.errors) window.location.replace('/');
    });
  return (
    <>
      <h2>Login</h2>
      <button type="button" data-testid="login-button" onClick={popLogin}>
        Login With Phone
      </button>
    </>
  );
};

Login.propTypes = {
  login: func.isRequired,
};

export default compose(graphql(LOGIN, { name: 'login' }))(Login);
