import React from 'react';
import Main from '../layouts/Main';
import MenuItems from '../components/MenuItems';

const Menu = () => (
  <Main>
    <p>Menu Page</p>
    <MenuItems />
  </Main>
);

Menu.secure = true;

export default Menu;
