import React from 'react';
import Main from '../layouts/Main';
import UserDetails from '../containers/UserDetails';

const Profile = () => (
  <Main>
    <h2>Profile Page</h2>
    <UserDetails />
  </Main>
);

Profile.secure = true;

export default Profile;
