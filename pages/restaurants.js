import React, { useState } from 'react';
import Main from '../layouts/Main';
import AddRestaurant from '../containers/restaurants/AddRestaurant';
import RestaurantList from '../containers/restaurants/RestaurantList';

const Restaurants = () => {
  const [editting, setEditting] = useState(false);
  const addRestaurant = () => setEditting(true);
  const onCancel = () => setEditting(false);
  return (
    <Main>
      <p>Restaurants Page</p>
      <RestaurantList />
      {!editting ? (
        <button type="button" onClick={addRestaurant}>
          Add Restaurant
        </button>
      ) : (
        <AddRestaurant onCancel={onCancel} onSuccess={onCancel} />
      )}
    </Main>
  );
};

Restaurants.secure = true;

export default Restaurants;
