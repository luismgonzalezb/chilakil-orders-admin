const next = require('next');
const { ApolloServer } = require('apollo-server-express');
const typeDefs = require('./lib/server/graphql/typeDefs');
const resolvers = require('./lib/server/graphql/resolvers');

const app = require('./app');
const passport = require('./lib/server/passport');
const { getChilakilApiClient } = require('./lib/server/ChilakilApiClient');

const chilakilApi = getChilakilApiClient();

// ################################################################
// NEXT APP CONFIG
// ################################################################
const dev = process.env.NODE_ENV !== 'production';
const port = parseInt(process.env.PORT, 10) || 3000;
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();

nextApp
  .prepare()
  .then(() => {
    // ################################################################
    // STATIC FILES BEFORE SESSION MANAGEMENT
    // ################################################################
    app.get('/_next/*', (req, res) => handle(req, res));
    // ################################################################
    // INIT SESSION MANAGEMENT
    // ################################################################
    app.use(passport.initialize());
    app.use(passport.session());
    app.post('/login_auth', (req, res, nxt) => {
      passport.authenticate('accountkit', (err, user) => {
        if (err) return nxt(err);
        if (!user) return res.redirect('/login');
        return req.logIn(user, _err => (_err ? nxt(_err) : res.send({ user })));
      })(req, res, nxt);
    });
    app.get('/logout', (req, res) => {
      req.logout();
      res.redirect('/');
    });
    // ################################################################
    // GRAPHQL CONFIG
    // ################################################################
    const server = new ApolloServer({
      typeDefs,
      resolvers,
      dataSources: () => ({ chilakilApi }),
      context: async ({ req }) => ({
        token: await chilakilApi.getSessionJwtToken(
          req.session && req.session.user,
          req,
        ),
      }),
    });
    server.applyMiddleware({ app });
    // ################################################################
    // HANDLE THE REST OFF THE ROUTES
    // ################################################################
    app.get('*', (req, res) =>
      req.url !== '/login' && !req.isAuthenticated
        ? res.redirect('/login')
        : handle(req, res),
    );
    app.listen(port, err => {
      if (err) throw err;
      console.log(`> Ready on ${port}`);
    });
  })
  .catch(ex => {
    process.exit(1);
    console.error(ex.stack);
  });
