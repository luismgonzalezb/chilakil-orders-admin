export const IS_LOGGED_IN_DATA = {
  session: {
    isLoggedIn: true,
  },
};

export const IS_LOGGED_OUT_DATA = {};
