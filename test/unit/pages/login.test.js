import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { render, cleanup, waitForElement } from 'react-testing-library';
import Login from '../../../pages/login';

afterEach(cleanup);

describe('<Login />  Should render with Apollo mock provider', () => {
  test('it should render correctly', async done => {
    const { getByTestId } = render(
      <MockedProvider mocks={[]}>
        <Login />
      </MockedProvider>,
    );
    const loginButton = await waitForElement(() => getByTestId('login-button'));
    expect(loginButton).not.toBeNull();
    done();
  });
});
